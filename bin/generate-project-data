#!/usr/bin/env ruby

$LOAD_PATH.unshift File.expand_path('../lib', __dir__)
$stdout.sync = true

require 'chronic_duration'
require 'data_generator'
require 'gitlab'
require 'optimist'
require 'rainbow'
require 'uri'

opts = Optimist.options do
  banner "\nGenerate Data for GitLab Project."
  banner "Usage: generate-project-data [options]"
  banner "\nOptions:"
  opt :environment_url, "The environment's full URL", short: :none, type: :string, required: true
  opt :project_id, "Project ID to generate the data against", short: :none, type: :string
  opt :merge_requests, "Number of merge requests to create", type: :integer
  opt :issues, "Number of issues to create", type: :integer
  opt :labels, "Number of labels to create", type: :integer
  opt :comments, "Number of comments to create for every merge request and / or issue", type: :integer, default: 20
  opt :closed_merge_requests, "Percentage of merge requests to create as closed", type: :integer
  opt :closed_issues, "Percentage of issues to create as closed", type: :integer
  opt :releases, "Create releases from all existing tags", type: :flag, default: false
  opt :force, "Skip the data injection warning", type: :flag, default: false
  opt :pool_size, "The pool size of threads to use for concurrent API calls", type: :integer, default: 100
  opt :help, 'Show help message'
  banner "\nEnvironment Variables:"
  banner "  ACCESS_TOKEN             A valid GitLab Personal Access Token for the specified environment. The token should come from a User that has admin access for the project and have API permission. (Default: nil)"
  banner "\nExamples:"
  banner "  Generate 100 merge requests with default 20 comments in each:"
  banner "  ./bin/generate-project-data --environment-url <URL> --project-id 12345 --merge-requests 100"
  banner "  Generate 200 merge requests and 100 issues with 50 labels and 30 comments in each:"
  banner "  ./bin/generate-project-data --environment-url <URL> --project-id 12345 --merge-requests 200 --issues 100 --labels 50 --comments 30"
  banner "  Generate 200 merge requests and 100 issues with 20% and 40% closed respectively:"
  banner "  ./bin/generate-project-data --environment-url <URL> --project-id 12345 --merge-requests 200 --issues 100 --closed-merge-requests 20 --closed-issues 40"
end

raise 'Environment Variable ACCESS_TOKEN must be set to proceed. See command help for more info' unless ENV['ACCESS_TOKEN']

unless opts[:force]
  puts Rainbow("The Data Generator Tool will inject the data into the specified project ##{opts[:project_id]} on #{opts[:environment_url]}. Do you want to proceed? [Y/N]").yellow
  prompt = $stdin.gets.chomp
  unless prompt.match?(/y(?:es)?|1/i)
    puts Rainbow('Data generation aborted.').green
    exit
  end
end

data_generator = DataGenerator.new(opts[:environment_url], opts[:project_id], opts[:pool_size])
# Check that environment can be reached and that token is valid
headers = {
  Authorization: "Bearer #{ENV['ACCESS_TOKEN']}"
}
puts "Checking that GitLab environment '#{opts[:environment_url]}' is available and that provided Access Token works..."
check_res = data_generator.make_http_request(method: 'get', url: URI.join(opts[:environment_url], '/api/v4/version').to_s, headers:)
raise "Environment check has failed:\n#{check_res[:status]} - #{check_res[:body]}" if check_res[:status].client_error? || check_res[:status].server_error?

version = check_res[:body].values.join(' ')
puts "Environment and Access Token check was successful - URL: #{opts[:environment_url]}, Version: #{version}\n\n"

start_time = Time.now.to_i
puts Rainbow("Starting to generate data for the project #{data_generator.project.name}...").color(230, 83, 40)
begin
  labels = data_generator.generate_labels(labels_count: opts[:labels])
  data_generator.generate_merge_requests(merge_requests_count: opts[:merge_requests], labels:, closed_merge_requests: opts[:closed_merge_requests], merge_request_comments_max_count: opts[:comments])
  data_generator.generate_issues(issues_count: opts[:issues], closed_issues: opts[:closed_issues], labels:, issue_comments_max_count: opts[:comments])
  data_generator.generate_releases_from_tags if opts[:releases]

  run_time = ChronicDuration.output(Time.now.to_i - start_time, format: :long)
  puts Rainbow("Data generation finished after #{run_time}").green

rescue Interrupt
  warn Rainbow("\nCaught the interrupt. Stopping.").yellow
  exit
rescue StandardError => e
  warn Rainbow("\nData generation failed:\n #{e.message}\n #{e.backtrace}").red
end
