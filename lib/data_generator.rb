require 'chronic_duration'
require 'faker'
require 'http'
require 'gitlab'
require 'json'
require 'optimist'
require 'ruby-progressbar'
require 'tty-spinner'
require 'parallel'

class DataGenerator
  attr_reader :project

  def initialize(environment_url, project_id, pool_size)
    @gitlab_client = Gitlab.client(
      endpoint: "#{environment_url}/api/v4",
      private_token: ENV['ACCESS_TOKEN']
    )
    @project = @gitlab_client.project(project_id)
    @pool_size = pool_size
  end

  ########## Merge Requests ###########

  def generate_merge_requests(merge_requests_count:, labels:, closed_merge_requests:, merge_request_comments_max_count:)
    if merge_requests_count.nil?
      make_http_request(method: 'get', url: project._links.merge_requests, params: { page: 1, per_page: 100 })[:body]
    else
      merge_requests = []

      # Get some existing files to update them in commits
      files = @gitlab_client.tree(project.id, per_page: 100, ref: 'main').select { |file| file.type == "blob" }.sample(20)

      Parallel.map(1..merge_requests_count, in_threads: @pool_size, progress: { title: 'Generating merge requests', format: "%t | %c/%u %p% |%E |%b>%i| %a" }) do
        merge_requests << create_merge_request(labels:, file_path: files.sample.path)
      rescue StandardError => e
        if e.message.match?(/Failed to open TCP connection|You can only create or edit files when you are on a branch|running pre-receive hooks|Failed to create branch|i\/o timeout|no such file or directory|running post-receive hooks|500 Internal Server Error/)
          sleep 2
          retry
        end

        raise e
      end

      generate_merge_request_comments(merge_requests:, merge_request_comments_max_count:)
      close_entities(api_url: project._links.merge_requests, percentage_to_close: closed_merge_requests, entities: merge_requests) unless closed_merge_requests.nil?
    end
  end

  def create_merge_request(labels:, file_path:)
    new_branch = @gitlab_client.create_branch(project.id, Faker::Lorem.words(number: 5).join('-'), 'main')
    generate_commits(branch: new_branch.name, file_path:)
    # Pause for 2 seconds to let branches create fully on env
    sleep 2
    mr_labels = get_random_labels(labels:)
    body = {
      source_branch: new_branch.name,
      target_branch: 'main',
      description: Faker::Markdown.unique.sandwich(sentences: 5),
      labels: mr_labels
    }
    @gitlab_client.create_merge_request(project.id, Faker::Lorem.sentence, body)
  end

  def generate_merge_request_comments(merge_requests:, merge_request_comments_max_count:)
    min_comments_count = (ENV['MIN_COMMENTS_COUNT'] || 1).to_i

    Parallel.map(merge_requests, in_threads: @pool_size, progress: { title: 'Generating comments for MRs', format: "%t | %c/%u %p% |%E |%b>%i| %a" }) do |merge_request|
      rand(min_comments_count..merge_request_comments_max_count).times do
        comment_body = Faker::Markdown.unique.sandwich(sentences: 5)
        @gitlab_client.create_merge_request_comment(project.id, merge_request.iid, comment_body)
      end
    rescue StandardError => e
      retry if e.message.include?("Failed to open TCP connection")

      raise e
    end
  end

  ########## Commits for Merge Request ###########

  def generate_commits(branch:, file_path:)
    min_commits_count = (ENV['MIN_COMMITS_COUNT_IN_MR'] || 1).to_i
    max_commits_count = (ENV['MAX_COMMITS_COUNT_IN_MR'] || 5).to_i
    rand(min_commits_count..max_commits_count).times do
      create_commit(branch:, file_path:)
    end
  end

  def create_commit(branch:, file_path: nil, options: {})
    actions = prepare_commit_actions(file_path:)
    body = {
      branch:,
      commit_message: Faker::Lorem.sentence,
      actions:
    }
    @gitlab_client.post("/projects/#{project.id}/repository/commits", body: options.merge(body))
  end

  def prepare_commit_actions(file_path:)
    min_actions_count = (ENV['MIN_ACTIONS_COUNT_IN_COMMIT'] || 1).to_i
    max_actions_count = (ENV['MAX_ACTIONS_COUNT_IN_COMMIT'] || 5).to_i
    actions = []
    rand(min_actions_count..max_actions_count).times do |number|
      action = {
        action: "create",
        file_path: "#{Faker::Lorem.words(number: 7).join('_')}_#{number}.md",
        content: Array.new(rand(5..10)) { Faker::Markdown.unique.sandwich(sentences: 1) }.join
      }
      actions << action
    end
    update_action = {
      action: "update",
      file_path:,
      content: Array.new(rand(5..10)) { Faker::Markdown.unique.sandwich(sentences: 1) }.join
    }
    actions << update_action unless file_path.nil?
    actions
  end

  ########## Close Merge Requests / Issues ##########

  def close_entities(api_url:, entities:, percentage_to_close:)
    ratio_to_close = percentage_to_close.to_f / 100
    total_entities_to_close = (entities.count * ratio_to_close).to_i
    entity_name = api_url.split('/').last

    Parallel.map(entities.sample(total_entities_to_close), in_threads: @pool_size, progress: { title: "Closing #{total_entities_to_close} #{entity_name}", format: "%t | %c/%u %p% |%E |%b>%i| %a" }) do |entity|
      make_http_request(method: 'put', url: "#{api_url}/#{entity.iid}", params: { state_event: 'close' })
    end
  end

  ########## Issues ###########

  def generate_issues(issues_count:, closed_issues:, labels:, issue_comments_max_count:)
    return false if issues_count.nil?

    issues = []

    Parallel.map(1..issues_count, in_threads: @pool_size, progress: { title: 'Generating issues', format: "%t | %c/%u %p% |%E |%b>%i| %a" }) do
      options = {}
      options['description'] = Faker::Markdown.unique.sandwich(sentences: 5)
      options['labels'] = get_random_labels(labels:) unless labels.nil?
      issues << @gitlab_client.create_issue(project.id, Faker::Lorem.sentence, options)
    end

    generate_issue_comments(issues:, issue_comments_max_count:)
    close_entities(api_url: project._links.issues, percentage_to_close: closed_issues, entities: issues) unless closed_issues.nil?
  end

  def generate_issue_comments(issues:, issue_comments_max_count:)
    min_comments_count = (ENV['MIN_COMMENTS_COUNT'] || 1).to_i

    Parallel.map(issues, in_threads: @pool_size, progress: { title: 'Generating comments for Issues', format: "%t | %c/%u %p% |%E |%b>%i| %a" }) do |issue|
      rand(min_comments_count..issue_comments_max_count).times do
        body = Faker::Markdown.unique.sandwich(sentences: 5)
        create_issue_comment(issue_id: issue.iid, body:)
      end
    rescue StandardError => e
      retry if e.message.include?("Failed to open TCP connection")

      raise e
    end
  end

  ########## Labels ###########

  def generate_labels(labels_count:)
    if labels_count.nil?
      get_all_entities(api_url: project._links.labels)
    else
      labels = []

      Parallel.map(1..labels_count, in_threads: @pool_size, progress: { title: 'Generating labels', format: "%t | %c/%u %p% |%E |%b>%i| %a" }) do
        label_name = Faker::Lorem.words(number: 2).join('-')
        labels << @gitlab_client.create_label(project.id, label_name, Faker::Color.hex_color)
      rescue StandardError => e
        retry if e.message.include?("already exists")

        raise e
      end

      labels
    end
  end

  ########## Releases ###########

  def generate_releases_from_tags
    existing_tags = get_all_entities(api_url: "#{project._links.self}/repository/tags?sort=asc")

    Parallel.map(existing_tags, in_threads: @pool_size, progress: { title: 'Generating releases', format: "%t | %c/%u %p% |%E |%b>%i| %a" }) do |tag|
      if tag["release"].nil?
        description = "#{Faker::Markdown.headers}\n#{Faker::Markdown.emphasis}\n#{Faker::Markdown.ordered_list}\n#{Faker::Markdown.table}\n#{Faker::Markdown.sandwich(sentences: 6, repeat: 5)}"
        @gitlab_client.create_release(project.id, tag["name"], description)
      end
    end
  end

  ########## Helpers ###########

  def get_random_labels(labels:)
    labels_count = (ENV['MAX_LABELS_COUNT'] || 3).to_i
    if got_from_api?(labels)
      labels.sample(labels_count).map { |label| label["name"] }.join(',')
    else
      labels.map(&:name).sample(labels_count).join(',')
    end
  end

  def get_random_merge_request_id(merge_requests:)
    if got_from_api?(merge_requests)
      merge_requests.sample["iid"]
    else
      merge_requests.sample.iid
    end
  end

  def got_from_api?(entity)
    # Is entity generated now or it was fetched via API from existing ones with `get_all_entities`
    # "Generated now" are objects, "fetched via API" ones are hashes
    entity.first.is_a?(Hash)
  end

  def get_all_entities(api_url:)
    per_page = 100
    headers = make_http_request(method: 'get', url: api_url, params: { page: 1, per_page: })[:headers]
    total_entity_pages = headers["X-Total-Pages"].to_i
    total_entities = headers["X-Total"].to_i
    spinner = TTY::Spinner.new("[:spinner] Collecting #{total_entities} #{api_url.split('/').last}")
    spinner.auto_spin
    entities = []
    1.upto(total_entity_pages) do |page_no|
      entity_response = make_http_request(method: 'get', url: api_url, params: { page: page_no, per_page: })
      entities.concat(entity_response[:body])
    end
    spinner.success

    entities
  end

  ########## Requests ###########

  def create_issue_comment(issue_id:, body:)
    @gitlab_client.post("/projects/#{project.id}/issues/#{issue_id}/discussions", body: { body: })
  end

  def make_http_request(method: 'get', url: nil, params: {}, headers: {}, fail_on_error: true)
    headers = { 'PRIVATE-TOKEN': ENV['ACCESS_TOKEN'] }.merge(headers)
    raise "URL not defined for making request. Exiting..." unless url

    res = HTTP.follow.method(method).call(url, form: params, headers:)

    raise "#{method.upcase} request failed!\nCode: #{res.code}\nResponse: #{res.body}\n" if fail_on_error && !res.status.success?

    { headers: res.headers, body: JSON.parse(res.body), status: res.status }
  end
end
